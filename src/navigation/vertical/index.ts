export default [
  {
    title: "Dashboard",
    to: { name: "dashboard" },
    icon: { icon: "tabler-smart-home" },
  },
  {
    title: "Main features",
    to: { name: "main" },
    icon: { icon: "tabler-api-app" },
  },
];
